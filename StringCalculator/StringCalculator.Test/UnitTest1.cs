using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace StringCalculator.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void EmptyReturnsZero()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value = sc.Calculate("");

            // Assert
            Assert.AreEqual(0, value);
        }
        [TestMethod]
        public void SingleNumberReturnItsValue()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value4 = sc.Calculate("4");
            var value27 = sc.Calculate("27");

            // Assert
            Assert.AreEqual(4, value4);
            Assert.AreEqual(27, value27);
        }
        [TestMethod]
        public void TwoNumbersSeparatedWithDotReturnSum()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value4_5 = sc.Calculate("4,5");

            // Assert
            Assert.AreEqual(9, value4_5);
        }
        [TestMethod]
        public void TwoNumbersSeparatedWithNewLineReturnSum()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value4_5 = sc.Calculate("4,5");

            // Assert
            Assert.AreEqual(9, value4_5);
        }
        [TestMethod]
        public void NumbersSeparatedWithNewLineOrDotReturnSum()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value25 = sc.Calculate("4,5\n6,10");

            // Assert
            Assert.AreEqual(25, value25);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeNumberException()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value = sc.Calculate("4,5\n6,-10");

            // Assert

        }
        [TestMethod]
        public void ValuesGreaterThan1000Ignored()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value50 = sc.Calculate("4,5\n6,10,1001\n34567,25");

            // Assert
            Assert.AreEqual(50, value50);
        }
        [TestMethod]
        public void AdditionalSingleCharDelimiter()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value50 = sc.Calculate("//#\n4,5\n6,10,1001#34567,25#1000000");

            // Assert
            Assert.AreEqual(50, value50);
        }
        [TestMethod]
        public void AdditionalMultipleCharDelimiter()
        {
            // Arrange
            var sc = new StringCalculator();

            // Act
            var value50 = sc.Calculate("//[###]\n4,5\n6,10,1001###34567,25###1000000");

            // Assert
            Assert.AreEqual(50, value50);
        }
        //[TestMethod]
        //public void AdditionalSingleOrMultipleCharDelimiters()
        //{
        //    // Arrange
        //    var sc = new StringCalculator();

        //    // Act
        //    var value50 = sc.Calculate("//[T][#T#]\n4,5\n6,10,1001###34567,25###1000000");

        //    // Assert
        //    Assert.AreEqual(50, value50);
        //}
    }
}
